# GT Discord Bot

This bot was originally built to be a utility bot for the [Official Game Theory Discord](https://discord.gg/gametheorist). Now it has morphed into a monster beyond our control. 

# Requirements

This bot uses four (4) main software components:

- [Python 3](https://www.python.org/) - The programming language the bot is made in. The official server version used is 3.6
- [Poetry](https://github.com/sdispater/poetry) - Poetry works as a replacement to `pip`. Poetry also provides built in virtual environment and version pinning.
- [Discord.py](https://discordpy.readthedocs.io/en/latest/) - Discordpy is the `discord` package used in the code. This package lets us communicate with discord.
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/) - Git is used to share and manage the code of this project.

## Optional Requirements
Some of the Plugins/Cogs require some additional software:

- Redis/aioredis - Used for large dictionary/hash storage.
- Postgres/aiopg - Used to store long-term data, like Question of the Day.

# Installation

[First install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/) and [setup a gitlab account](https://gitlab.com/). Gitlab is completely free.

[Then make sure Python 3 is installed.](https://realpython.com/installing-python/)

If `pip` is already installed move forward with [installing Poetry](https://python-poetry.org/docs/#installation), if you do not know what poetry is or why you are using it [read this article](https://realpython.com/effective-python-environment/).

**Note:** If you have multiple versions of Python installed on your computer, you may need to run `pip3` instead of `pip` to install Poetry.

[Next you can fork the repository](https://tharis63.medium.com/git-fork-vs-git-clone-8aad0c0e38c0) by clicking the "Fork" button on the top right hand corner of the [project page](https://gitlab.com/jtiki/gt-vegas).

Once the project has been forked, you now have a "copy" of the bot under your personal gitlab account. You are able to make changes to this version then request the changes be "pulled" or "merged" into the main bot library through a "Merge/Pull Request."

---
### Side
If you haven't yet, take a few minutes to [learn some git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html), theres a few more great resources:

- https://try.github.io/
- https://learngitbranching.js.org/
- https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
- https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/
---

Clone the repo to your machine and navigate to the folder containing `pyproject.toml`, run the following command:

```python
poetry install
```

If you have everything installed correctly Poetry should setup a new virtual envirtonment and install all of the dependencies.

---
### Poetry Issue with PATH on Windows
If you're getting an error like `poetry : The term 'poetry' is not recognized as the name of a cmdlet, function, script file, or operable program. Check the spelling of the name, or if a path was included, verify that the path is correct and try again.`, follow these steps:

Run `$Env:Path` in PowerShell, and if you do not see `C:\Users\YourUserFolder\.poetry\bin`, you need to manually add this into your PATH.

To do this, follow the steps here: https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/ and add `C:\Users\YourUserFolder\.poetry\bin` in your PATH.

After doing this, restart PowerShell and run `$Env:Path` to see if it's been added in and run `poetry --version` to make sure poetry is properly installed.
---

Copy `settings/local_settings.py.ex` to `settings/local_settings.py` and update the following settings:

- `TOKEN` - your discord bot token, [learn how to generate a token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token)
- `GUILD_ID` - the default server/guild id for `event_logger`, some events do not include a way to trace the guild.
- `POSTGRES_AUTH` - postgres information if you will use those COGs
- `COGS` - a list of whatever plugins/COGs you will load

# Contributing

This bot uses discord.py so make sure you are familiar with that library. 

This project has four main sections:

## bot.py 

This file contains the VEGAS class that is the main class of our bot. The `__init__` method on `VEGAS` also contains the plugin loading logic as well as a custom `dm_author` method that demonstrates a simple bot command.

## run.py

This file is used to start the bot. It will import from your custom `settings.local_settings` (next section) for a `TOKEN` constant you provide as well as postgres information (check `settings.base` for more options). The rest of the functions are just simple helper methods and starting the main event loop.

## settings

The `settings` folder contains one file `base.py` and expects you to provide one file `local_settings.py`. `base.py` contains all settings for the bot that are not private (tokens) or personal (absolute file paths), that type of information should be placed in `settings/local_settings.py`. For the bot to start and work properly you must create and setup your own `local_settings` file. By default `local_settings` will be expected to provide a valid Discord Bot Token and aiopg connection string. These two settings should be stored in `TOKEN` and `POSTGRES_AUTH` respectively. Lastly `local_settings` will need to import all items in `settings.base`, this can be accomplished with `from .base import *`.

By default `settings/local_settings.py` is ignored by git, so you can save sensitive info, like passwords, to this file.

## plugins

Where all plugin files are stored.

# Plugin Basics

`discord.py` provides a great method off of `discord.ext.commands.Bot` called `add_cog`. The `add_cog` method allows us to add all of the methods/commands from one class onto our bot. This method is greatly prefered as opposed to having to define every method/command in one file in one class. Breaking up our code like this keeps everything much more readable.

A sample of a simple plugin can be seen in `plugins/memes.py` and `plugins/notification.py`.
