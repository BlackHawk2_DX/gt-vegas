"""This file provides multiple helper `checks` that make dealing with discord.py easier."""
import functools

import discord
from discord.ext import commands

def has_user_ping():
    async def predicate(ctx):
        if ctx.message.mentions:
            return True
        await ctx.send("You have to provide a user ping!")
        return False
    return discord.ext.commands.check(predicate)

def doesnt_have_role(item):
    def predicate(ctx):
        if not isinstance(ctx.channel, discord.abc.GuildChannel):
            raise discord.ext.commands.NoPrivateMessage()

        if isinstance(item, int):
            role = discord.utils.get(ctx.author.roles, id=item)
        else:
            role = discord.utils.get(ctx.author.roles, name=item)
        if role is None:
            return True
        return False

    return discord.ext.commands.check(predicate)

def only_channel(item):
    def predicate(ctx):
        if not isinstance(ctx.channel, discord.abc.GuildChannel):
            raise discord.ext.commands.NoPrivateMessage()

        if isinstance(item, int):
            return item == ctx.message.channel.id
        else:
            return item == ctx.message.channel.name

    return discord.ext.commands.check(predicate)
