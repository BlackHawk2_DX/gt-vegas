import discord
from discord.utils import get
from discord.ext import commands
from settings.local_settings import BOT_PREFIX
from discord.ext.commands import has_any_role

LOG_CHANNEL_NAME = 'vegas-logging'

DETECTION_COMMAND = tuple([prefix + "forms" for prefix in BOT_PREFIX])


class BotFormsPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.form_cache = None
        self.form_cache_sql = "SELECT * FROM bot_forms_botform;"
        self.sql_insert = "INSERT INTO bot_forms_botform (name, link) VALUES (%s, %s);"
        self.sql_insert_requestor = "INSERT INTO bot_forms_botformresult (discord_user_id, form_id) VALUES (%s, %s);"
        self.sql_results = "SELECT * FROM bot_forms_botformresult WHERE form_id=(select id from bot_forms_botform where name='{}')"

    def event_logging_channel(self, channels):
        return get(channels, name=LOG_CHANNEL_NAME)

    @commands.group(description="Server Forms Galore!",
                    case_insensitive=True,
                    aliases=['f',])
    async def forms(self, context):
        pass

    @forms.command(name='get_results',
                  hidden=True,
                  aliases=['r',])
    async def get_results(self, context, form_slug):
        await self._check_refresh_form_cache()
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(self.sql_results.format(form_slug.lower()))
                results = await cur.fetchall()
        embed_desc = ""
        for item in results:
            embed_desc += "<@{}>\n".format(str(item[1]))
        embed_title = "Users Who Requested {}".format(form_slug)
        embed = discord.Embed(title=embed_title, description=embed_desc, color=58655)
        await context.send(embed=embed)

    @forms.command(name='get_form',
                  description='Request a form be DM\'d to you to fill out!',
                  brief='Get a form',
                  aliases=['g',])
    async def get_form(self, context, form_slug):
        await self._check_refresh_form_cache()
        form_found = False
        for form in self.form_cache:
            if form[1] == form_slug:
                requested_form = form
                form_found = True

        if not form_found:
            await self.event_logging_channel(
                    context.message.guild.text_channels
                ).send(
                    "Requested " + form_slug + ", but couldn't find a form like that!")
        else:
            requestor = context.message.author
            requested_form_id = requested_form[0]
            requsted_form_link = requested_form[2]
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(self.sql_insert_requestor, [requestor.id, requested_form_id])
                    await requestor.send(requsted_form_link)




    @forms.command(name='list_forms',
                  description='List all available Forms for server members to fill out!',
                  brief='Lists all forms',
                  aliases=['l',])
    async def list_forms(self, context):
        await self._check_refresh_form_cache()
        embed_desc = ""
        for form in self.form_cache:
            embed_desc += form[1] + "\n"
        embed = discord.Embed(name="Sever Forms!", description=embed_desc, color=58655)
        await context.send(embed=embed)


    @forms.command(name='add_form',
                  description='Add a Form for server members to fill out!',
                  brief='Adds a form (staff only)',
                  aliases=['a', '+'])
    @has_any_role('Mods', 'Disaster Director')
    async def add_form(self, context, name, link):
        if not name:
            await context.send("Please provide a form name with no spaces or special characters!")
        if not link:
            await context.send("Please provide a link to a form!")

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(self.sql_insert, [name.lower(), link])
                await context.send("Form accepted!")
        message = await context.send("Refreshsing form cache!")
        self.form_cache = None
        await self._check_refresh_form_cache()
        await message.edit(content="Done.")


    async def _check_refresh_form_cache(self):
        if not self.form_cache:
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(self.form_cache_sql)
                    self.form_cache = await cur.fetchall()


    @forms.command(name='refresh_form_cache',
                   hidden=True,
                   aliases=['rfc'])
    async def refresh_form_cache(self, context, *args, **kwargs):
        message = await context.send("Fetching form cache!")
        self.form_cache = None
        await self._check_refresh_form_cache()
        await message.edit(content=":thumbsup:")



def setup(bot):
    bot.add_cog(BotFormsPlugin(bot))
