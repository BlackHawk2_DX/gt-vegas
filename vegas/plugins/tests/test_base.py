from plugins.base import BasePlugin


def test_base_plugin_init():
    result = "Fake bot"
    base = BasePlugin(result)
    assert base.bot == result
