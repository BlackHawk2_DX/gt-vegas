from discord.ext import commands
from aioredis.commands import HashCommandsMixin


REDIS_KEY = "gt:command_permissions:"

def flexible_permissions():
    async def predicate(ctx):
        func_name = ctx.command.qualified_name.replace(" ", "_")
        caller = ctx.message.author
        roles = [role.name for role in caller.roles]

        permissions = await postgres_fetch_permissions(ctx.bot, func_name)
        # Sort returned postgres values into easier access variable names
        allowed_roles = permissions[2]
        ignored_roles = permissions[3]
        allowed_channels = permissions[4]
        ignored_channels = permissions[5]

        if allowed_roles:
            if not bool(set(allowed_roles) & set(roles)):
                await ctx.send("Sorry you don't have the roles to do that.")
                return False
        if ignored_roles:
            if bool(set(ignored_roles) & set(roles)):
                await ctx.send("Sorry you have a role that is preventing you from doing that")
                return False
        if allowed_channels:
            if not bool(set(allowed_channels) & set(ctx.message.channel)):
                await ctx.send("You can't do that in this channel!")
                return False
        if ignored_channels:
            if bool(set(ignored_channels) & set(ctx.message.channel)):
                await ctx.send("You can't do that in this channel!")
                return False

        return True

    return commands.check(predicate)

async def check_redis_cache(bot, func_name):
    redis_perms = await bot.redis.hgetall(REDIS_KEY + func_name)
    if list(redis_perms):
        return redis_perms
    await refresh_redis_cache_command(bot, func_name)

async def postgres_fetch_permissions(bot, func_name):
        sql_template = "SELECT * FROM public.flexible_permission_flexiblepermission WHERE bot_command='{}'"
        sql = sql_template.format(func_name)

        with (await bot.pg_pool.cursor()) as cur:
            await cur.execute(sql)
            results = await cur.fetchone()
            return results
